package com.darth.locationservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import java.util.Locale;

/**
 * Created by darth on 6/27/17.
 */

public class LocationChecker extends Service {

    private static final String TAG = LocationChecker.class.getSimpleName();

    /**
     * Variable to hold the location address extracted
     * using FetchAddressIntentService
     */
    private static String locationAddress = null;

    private LocationManager mLocationManager = null;

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        /**
         * Updates {@code mLastLocation} every time location changes
         */
        public LocationListener(String provider) {
            Log.e(TAG, String.format(Locale.ENGLISH, "%s: %s", "onLocationChanged", provider));
            mLastLocation = new Location(provider);
        }

        /**
         * Updates {@code mLastLocation} every time location changes
         */
        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, String.format(Locale.ENGLISH, "%s: %s", "onLocationChanged:", location));
            mLastLocation.set(location);

            // call the FetchAddressIntentService when there is a change in location
            // FetchAddressIntentService shall extract the location address from
            // the {@code location.getLatitude()} and {@code location.getLongitude()}
            startIntentService(location);
        }

        /**
         * Start FetchAddressIntentService to get location address
         * from {@code location.getLongitude() and location.getLatitude()}
         * for when onLocationChanged() is triggered
         * @param location
         */
        private void startIntentService(Location location) {
            // create the intent responsible for extracting location address
            Intent intent = new Intent(getBaseContext(), FetchAddressIntentService.class);

            // pass the result receiver to the service
            intent.putExtra(Constants.RECEIVER, LocationActivity.mResultReceiver);

            // pass the location data (latitude and longitude) to the service
            intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);

            // start the service
            startService(intent);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled:" + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled:" + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged:" + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate()
    {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            // register for location updates using the NETWORK_PROVIDER (WiFi),
            // LOCATION_INTERVAL (minimum time interval between location updates),
            // LOCATION_DISTANCE (minimum distance between location updates),
            // and mLocationListeners[1] ([1] is WiFi : listener whose onLocationChanged(Location) will be called
            // every time the location is updated)
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, Constants.LOCATION_INTERVAL, Constants.LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            // register for location updates using the NETWORK_PROVIDER (GPS),
            // LOCATION_INTERVAL (minimum time interval between location updates),
            // LOCATION_DISTANCE (minimum distance between location updates),
            // and mLocationListeners[0] ([0] is GPS : listener whose onLocationChanged(Location) will be called
            // every time the location is updated)
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, Constants.LOCATION_INTERVAL, Constants.LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    // remove the location updates request created by onCreate()
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    /**
     * Setter for location address when extracted by the FetchAddressIntentService
     * @param location
     */
    public static void setLocationAddress(String location) {
        locationAddress = location;
    }

    /**
     * Getter for location address to be sent via SMS
     * @return
     */
    public static String getLocationAddress() {
        return locationAddress;
    }
}
