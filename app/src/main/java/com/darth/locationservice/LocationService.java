package com.darth.locationservice;

/**
 * Created by darth on 6/29/17.
 */

import android.telephony.SmsManager;

/**
 * Class containing helper methods reused in the project
 */
public class LocationService {

    /**
     * Only parentPhoneNumber will be entertained by this intent service
     */
    private static String parentPhoneNumber;

    /**
     * Phone number to be used for sending SMS
     * Does not work with dual-sim phones
     */
    private static String serviceCentreNumber;

    /**
     * Sets the phone number to be used for sending SMS location updates
     * @param serviceCentreNumber
     */
    public static void setServiceCentreNumber(String serviceCentreNumber) {
        LocationService.serviceCentreNumber = serviceCentreNumber;
    }

    /**
     * Sets the recipient phone number for location updates
     * @param parentPhoneNumber
     */
    public static void setParentPhoneNumber(String parentPhoneNumber) {
        LocationService.parentPhoneNumber = parentPhoneNumber;
    }

    /**
     * Getter for service centre number
     * @return
     */
    public static String getServiceCentreNumber() {
        return LocationService.serviceCentreNumber;
    }

    /**
     * Getter for parent phone number
     * @return
     */
    public static String getParentPhoneNumber() {
        return LocationService.parentPhoneNumber;
    }

    /**
     * Sends the location update {@code messageBody} to {@ccode parentPhoneNumber}
     * using the {@code serviceCentreNumber}
     * @param parentPhoneNumber
     * @param serviceCentreNumber
     * @param messageBody
     */
    public static void sendSMS(String parentPhoneNumber, String serviceCentreNumber, String messageBody) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(parentPhoneNumber, serviceCentreNumber, messageBody, null, null);
    }
}
