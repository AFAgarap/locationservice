package com.darth.locationservice;

/**
 * Created by darth on 6/26/17.
 */

/**
 * Constant values reused in this project.
 */
final class Constants {
    static final int SUCCESS_RESULT = 0;

    static final int FAILURE_RESULT = 1;

    private static final String PACKAGE_NAME = "com.darth.locationservice";

    static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

    static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

    static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    static final int LOCATION_INTERVAL = 1000;

    static final float LOCATION_DISTANCE = 0;

    /**
     * Code used in requesting permissions
     */
    static final int REQUEST_PERMISSION_REQUEST_CODE = 34;

    /**
     * Constant used in the location settings dialog
     */
    static final int REQUEST_CHECK_SETTINGS = 0x1;

    static final String ADDRESS_REQUESTED_KEY = "address-request-pending";
    static final String LOCATION_ADDRESS_KEY = "location-address";

    /**
     * Interval for location updates. Inexact. Updates may be more or less frequent
     */
    static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = Constants.UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * Keys for storing activity state in a bundle
     */
    final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    final static String KEY_LOCATION = "location";
    final static String KEY_LAST_UPDATE_TIME_STRING = "last-updated-time-string";

}
