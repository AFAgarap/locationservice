package com.darth.locationservice;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import java.util.Locale;

import static com.darth.locationservice.LocationChecker.getLocationAddress;

/**
 * Created by darth on 6/27/17.
 */

public class SMSListener extends BroadcastReceiver {

    private static final String TAG = SMSListener.class.getSimpleName();

    protected static String messageBody;
    protected static String messageFrom;

    @TargetApi(19)
    @Override
    public void onReceive(Context context, Intent intent) {
        // messageBody must be cleared every time an SMS is received by the child app
        messageBody = "";

        // Checks whether there is a new SMS received by the device
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            // getMessagesFromIntent() gets the messages from the received SMS
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                // gets the address from whom the message is from
                messageFrom = smsMessage.getOriginatingAddress();
                // Checks whether the message is from the registered parent phone number
                // which is set in LocationActivity.java [onCreate()]
                // using the setter from LocationService (setParentPhoneNumber(String parentPhoneNumber))
                if (messageFrom.equals(LocationService.getParentPhoneNumber())) {
                    // stores the extracted message
                    messageBody += smsMessage.getMessageBody();
                }
            }
            // Checks whether the message is a request for location update
            // the string must be "locationUpdateRequest"
            if (messageBody.equals(context.getString(R.string.location_update_request))) {
                try{
                    // if the message is a location update request,
                    // send the last known location to the {@code LocationService.getParentPhoneNumber()}
                    // using the phone number of the device {@code LocationService.getServiceCentreNumber()}
                    LocationService.sendSMS(
                            LocationService.getParentPhoneNumber(),
                            LocationService.getServiceCentreNumber(),
                            String.format(Locale.ENGLISH, "%s: %s", "location", getLocationAddress()));
                    Log.i(TAG, "sent");
                } catch (Exception e) {
                    Log.e("Exception caught: ", e.getMessage());
                }
            }
        }
        Log.i(TAG, String.format(Locale.ENGLISH, "Message from %s: %s", messageFrom, messageBody));
    }
}