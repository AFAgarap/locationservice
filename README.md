# LocationService #

LocationService is an Android app developed for tracking the location of a *child* device through SMS updates. Location updates every one second (10000 milliseconds).

### Technical Specifications ###

* Minimum SDK version: API Level 19 (Android 4.4 KitKat)
* Maximum SDK version: API Level 26 (Android 8.0 O)
* SDK Tool(s): Google Repository version 54 (for Google Play Services)

### Features ###

* Gets the coordinates of the current location of the phone.
* Extracts the location address from the said coordinates.
* Listens for SMS request on location updates.
* Sends a location address update when there is an SMS request.
